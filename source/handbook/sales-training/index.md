---
layout: markdown_page
title: "Sales Training"
---

* [5 Signs You’re Talking At (Not Talking To) Your Prospects](#signs)
* [6 Ways to Make Your Sales Email Insanely Valuable to Prospects](#insanely)
* [3 Seemingly Scary Words Reps Shouldn’t Be Afraid to Use](#scary)
* [6 Questions Sales Reps Should (But Don’t) Ask Themselves Before Sending an Email](#questions)
* [4 Ways to Ruin a Sales Presentation](#presentation)
* [Persistent or Just Plain Pushy? 7 Sales Behaviors Decoded](#behaviors)


### 5 Signs You're Talking At (Not Talking To) Your Prospects <a name="signs"></a>

You’d think that for a salesperson, talking a lot about their product would be the best way to win a deal. After all, how can prospects decide to buy (or not) unless they know what they’re paying for?

Yet one of sales’ biggest ironies is that the more you talk about your product, the less likely you are to actually sell it. While speaking with your prospects is absolutely necessary to close sales, it’s all too easy to slip from talking to them into talking at them. The five signs below are easy-to-spot indicators that it’s time to let your prospect take center stage.

#### 1. You talk for more than half of each conversation.
In general, listening is more valuable to salespeople than talking. It’s the best, and in the early stages of a sales conversation, the only way to assess your prospect’s business pain, whether they’re being truthful, and their level of proficiency in implementing your product.

And listening early on sets the stage for the rest of your sales process. Don’t know what your prospect’s business strengths and weaknesses are because you never asked? You won’t be able to sell to their strengths or bridge the gap in weaker areas.

Can’t assess whether your prospect’s telling you the truth because you haven’t heard them speak enough to understand their voice tone? You won’t be able to distinguish between true blockers and brush-off objections.

Haven’t fully grasped the root causes of your prospect’s business pain? You won’t be able to position your product in a way that gets to the heart of a solution.

If you’re sensing a trend here, you’re right. Not listening enough severely limits your ability to sell successfully. It’s like trying to score a goal without knowing which team you play for.

#### 2. You’re telling more than asking.
But it’s not just how much you talk, but also what you say that’s important. It’s crucial to get your prospect talking as much as possible so you’re familiar with their personalities and problems. And this is hard to do if you’re not asking questions.

Of course, eventually you’ll have to talk more than you ask. Once you get a clear sense of the problem your prospect is trying to solve and their priorities, you’ll take the reins. But before that point, err on the side of asking more than you tell.

Dive deep into areas that are important for your prospect and make sure to clarify anything you don’t understand. This style lends itself to a slower sales process than showing up with a pitch ready, but you’ll set yourself up for success in the long run by building a foundational understanding of your prospect from day one.

#### 3. Your prospect isn’t engaging with you.
You (hopefully) wouldn’t continue to send email after email after email to a prospect who’d never opened or responded to a single message. And that principle applies to conversations as well.

If your prospect’s gone quiet, there’s a reason. Maybe they’re confused. Maybe you started talking about something completely irrelevant to their situation. Maybe you’re just not leaving them any opportunity to respond or ask questions.

Make a habit of pausing every few minutes to ask your prospect if they understand or simply give them an opening to speak. Taking your prospect’s temperature every so often is invaluable to making sales conversations helpful for them, and ultimately boosts your chances of closing the deal.

#### 4. Your statements could apply to any prospect.

Q: Why are conversations about the weather so boring to us?

A: Because they’re impersonal and could happen between any two random people.

If you find that you’re making generic statements and struggling to resonate, it’s because you haven’t done enough discovery. Gut check yourself by listening to call recordings or reviewing meetings while they’re still fresh in your mind. Did the insights you shared apply specifically to your prospect’s situation, or were they overly high-level and universally true?

If it's the latter, dig deeper. While many of your sales conversations will run along the same lines, the nuances and minutia of every situation will vary. If you can’t get right to the heart of your prospect’s concerns, take a beat to circle back to discovery.

#### 5. You can’t define your prospect’s problem.
Another way to check whether your sales conversations are productive for your prospect is to quiz yourself. Can you explain the deal to someone else on your team?

To test his understanding of a prospect’s problems, Dan Tyre, a sales director at HubSpot, tries to define three reasons his prospects would buy, three reasons his prospects would balk, and what the next step should be. If he can’t, he knows he needs to get on another call with his prospect to further understand what they want and discuss how to move forward in a mutually productive way.


### 6 Ways to Make Your Sales Email Insanely Valuable to Prospects <a name="insanely"></a>

The “just checking in” sales email isn’t just ineffective -- it’s also selfish.

Reps who send these types of emails aren’t offering any value. They’re trying to force or persuade buyers into replying to their message and making a purchase on the rep’s preferred timeline.

However, in today’s sales world, providing value to a customer throughout their individual buying process is the only way for a rep to close a deal. Hard selling tactics are bad for the buyer, and bad for the rep.

Modern reps are committed to providing guidance, information, and value to buyers whenever they reach out. They know it’s important the prospect sees just as much return on their time as the rep does.

Here are six easy ideas as to how you can provide value with each sales email you send.

#### 1. Attach a piece of content.
The easiest way to provide value to a buyer in a sales email is to include a piece of helpful content. Remember: The goal of an initial sales email is to not try to convert a buyer into a customer instantly, but to give them something that will truly interest them, and get a conversation going.

Spend some time researching the prospect’s company and observing their behavior on social media. Use this knowledge to offer a piece of content specific enough to their interests that they take the time to read it.

Email subject: a resource for you...

Hey [prospect],

Saw you sent out a tweet about [subject] and thought I’d pass along this blog post. It talks a lot about that subject, and provides some interesting takes and opinions. I would love to hear what you think about it.

All the best,

#### 2. Reference a mutual connection.
Having a mutual connection in common with a buyer can go a long way. In fact, a prospect is five times more likely to engage with a seller when they share a connection.

Surfacing a common connection is valuable to the buyer because it fosters trust. The buyer can now check with that mutual connection and determine whether or not this is the right product or service for them.

Email subject: I see we both know Leslie…

Hey [prospect],

I know you've been doing your research on GitLab, so I thought I'd put you in touch with someone we both know. [Mutual connection] and I have known each other for a while and they have been using GitLab, and it turns out you know each other too. 

Maybe all three of us can get together some time. Talk to you soon.

All the best,

#### 3. Offer a customer review.
Simply put, customers love reviews. How often do you check out Yelp before you make a decision on where to eat? How deep in the reviews do you go? I know I scan for a while, and I’m willing to bet you do too.

Learning what other people are saying about a product or service plays an important role in buying decisions. In fact, 88% of people take online reviews as seriously as they do personal recommendations, according to a study from Bright Local.

It’s clear that reviews are valuable for prospects, so why not send one along in your next email?

Email subject: the people have spoken

Hey [prospect],

I spent the weekend surfing the web, and came across some pretty cool reviews about what we’ve been talking about. I know you’ve been trying to solve [pain point] so I’m sending you these links to help you gather some more info.

Link 1
Link 2
Link 3

In these reviews, customers go into depth about how the product works and what they think of it. They’re honest, real, and I think you’ll appreciate them.

If you have any other questions, let me know. Talk to you soon.

#### 4. Send over a case study.
A case study can add instant value to a sales email. If the prospect is in a difficult spot and unsure how to proceed, sharing a story about a customer in a similar situation that describes how they solved their problem can be incredibly helpful.

Email subjectL I’ve seen this story before

Hey [prospect],

It’s funny, but after talking with you this last week I remembered that one of our customers was actually in the same situation you are in now. They were cruising along and crushing their market, but they were having problems with [pain point].

I attached their story to this email. It’s an interesting take, and I think it will provide a wealth of insight into how exactly they became one of the leaders in their market. Would love your feedback on it.

All the best,

#### 5. Provide a tactical suggestion.
If you notice an area of weakness in the buyer's business that you think you can help improve, let the prospect know. Offering insights not only strengthens your relationship, but it builds your credibility in their eyes.

Email subject: have you tried doing this…

Hey [prospect],

I was on your website earlier, and noticed a new blog post. I was wondering if you have ever tried out [suggestion]. It’s a really simple way to help blog posts rank better in search. It can also boost social shares, and help you build out your brand.

Here are a few examples of blog posts that use this technique:

Example 1
Example 2
Example 3

If you’re not sure what I mean, just give me a shout. Great post, by the way. I just shared it. Let’s connect soon.

All the best,

#### 6. Just respond.
Simply responding to a question, voicemail, tweet, or email is the easiest way of all to provide value. And keep in mind that you don’t have to give the final answer right away. A simple “I’m looking into this for you” will assure the prospect that you’re on the case.

The value of responding quickly extends beyond solving specific problems. According to Sprout Social, 26% of customers who don’t hear back from a company take to social media to post negative comments. Remember the importance of good reviews? Responding quickly plays a major role in prospects’ perception of your business.

Email subject: thanks for the question!

Hey [prospect],

Thanks for getting in touch. That’s a great question, and I’m glad you asked. I’m in the middle of a few meetings this morning, but I’ve sent your question along to our support experts (cc’d on this email). They’ll be able to help, and I’ll follow up with you at the end of the day.

Thanks again! If anything else comes up, please let me know.

All the best,


As author Michael Port recently wrote, “Give away so much value that you think you've given too much -- and then give more.”

Sales reps need to understand that the modern buyer is busy and doesn’t have time for “just checking in” emails. Strive to offer buyers an instant ROI with each and every message you send. 

Every word you say during a sales process is important. Every conversation you have is an opportunity to drive the process forward or derail it. With this in mind, it’s crucial to make sure these conversations count.

### 3 Seemingly Scary Words Reps Shouldn’t Be Afraid to Use <a name="scary"></a>
How often have you experienced an awkward silence during a sales call because the prospect asked a question you weren’t exactly sure how to respond to? If you’ve been in this position, don’t worry -- this happens on sales calls more than you think.

The common reaction for most sales reps is to get back to the parts of the service or product they are most knowledgeable about and move the conversation forward. But what about the question the prospect asked? If you just forget about it and keep talking about the benefits of your product, they'll decide to buy eventually -- right?

Probably not.

So if ignoring the question and hoping it goes away doesn’t work, what does? What should you do when the prospect poses a question you don’t quite know how to answer?

Say three short words:

“I don’t understand.”

While some reps might be afraid to say these words and reveal a chink in their “trusted advisor” armor, let me make the case for why this is the best option when you’re truly stumped.

#### “I don’t understand” allows you to clarify.
Understanding exactly what your prospect is saying allows you to avoid deal-disrupting miscommunications down the road. With this opening, the prospect can explain the question further so you can gather as much information as possible before responding.

#### "I don't understand" provides an opportunity to follow up.
In the event you get stumped with a prospect, saying you don’t know but will pursue the information with someone who does creates a perfect opportunity to follow up after the call is over. You can add value to your follow up by providing the requested information, while impressing the prospect with your diligence and commitment.

#### "I don't understand" allows you to gain new knowledge.
From now on you’ll know how to properly inform prospects seeking similar information. A knowledgeable sales rep is a trustworthy sales rep, and any opportunity you have to gain information is one you want to take advantage of.

#### "I don't understand" helps you become more likable.
Recent studies suggest that being honest makes you more likable. Instead of feeling like you don’t know what you’re talking about, a prospect will likely be impressed with a sales rep who admits that they don’t understand something. This proves your honesty and your willingness to be up front.

#### "I don't understand" enables you to ask better questions.
Instead of just moving on to the next part of the call, use “I don’t understand” to gather more information about your prospect and gain insight on how to refine your questions. Asking the right questions can help prospects paint a clear picture of their business, their plan, and their pain. With additional information, you can adjust your strategy to fit exactly what the buyer is looking for.

#### “I don’t understand” prompts them to use their own words.
Instead of trying to rephrase the prospect’s question for them, prompting them with “I don’t understand” gives the prospect the freedom to explain their situation and pain point in their own words. The rep can now get insight into how this prospect views their pain points, which is incredibly valuable when trying to tailor a solution and sell value.

Saying “I don’t understand” on a sales call isn’t the end of the world. Sales reps can strategically use this phrase to uncover a prospect’s meaning and give a genuine response. Remember, the prospect isn’t going to have all the answers. It’s okay if you don’t either. Put the focus on the relationship and providing value, and you’ll be okay.

Understand?

### 6 Questions Sales Reps Should (But Don’t) Ask Themselves Before Sending an Email <a name="questions"></a>
Few things top getting off on the right foot with a prospect. When you and the buyer connect off the bat, making the sale just comes naturally.

And where does that first impression happen? More often than not, online, with the first email you send. So here are a few things to ask yourself before you reach out to prospects for the first time to ensure your message will make a great first impression.

#### 1. Do I know their pain point?
A prospect’s pain is the primary reason they are going to make a purchase. Before writing that crucial first email, you need to understand how your product can help turn their pain point into a strength. If you don’t understand their pain point, or at least have a general idea of what they’re likely struggling with, it makes positioning your product as a solution difficult. Without knowing what it is they need to solve, how can you help them solve it?

To determine pain, commit to research. Study their business and look for a weakness or a point where they could improve.

#### 2. Do I know what’s happening in their market? 
Studying a prospect’s market gives you insight into their biggest challenges and where they need to improve. If you notice that some companies are hiring and expanding while others are cutting back, you have a clear picture of which companies are trending in the right direction.

By demonstrating knowledge of the prospect’s market in your initial email, you can build credibility with this person. You’re not only knowledgeable about their business, but you’ve taken the time to study their competitors and can now help implement an effective strategy for change.

#### 3. Will my name be familiar?
With an average response rate of 1.7%, the cold email is clearly losing its effectiveness.

Why? It lacks context. Today, we all screen our calls and emails. If we’re unfamiliar with the person reaching out to us, we’ll probably ignore them.

If the prospect doesn’t recognize your name, their initial reaction might be to hit the “report spam” button on your email. In sales, this a less-than-ideal outcome.

So instead of this person seeing your name for the first time in their inbox, aim to become familiar online first. To create rapport with a person before the first email, connect with them on social media, comment on a blog post they’ve written, or reach out to a mutual connection and ask for an introduction. All three of these techniques give you a chance to spark a discussion and make your name familiar.

#### 4. Do I have the right mindset?
Approaching a sale with the mindset of helping, guiding, and offering clarification is the mindset of the modern rep. According to CEB, today’s buyer is going through almost two-thirds of the journey without talking to anyone in sales. With this in mind, reps need to adjust and be ready to embrace the "always be helping" mentality, instead of "always be closing."

This takes the stigma of an annoying, pesky, or pushy sales rep out of the equation because you’re looking to guide prospects through their buying journey. If write your email thinking you have to close, you might shove your product down their throat or force them through the funnel faster than they want to go.

#### 5. What’s the value of this email?
What does the prospect get out of your message? What insights can you offer them in this email that they can’t get somewhere else?

Keep in mind that any touch can be the deciding factor behind whether or not this person buys from you. Bothering a prospect with an email they don’t need can be the downfall of a rep -- before the sale ever begins. When you’re able to offer value from the jump, you prompt responses and move the conversation forward.

Here are a few things you can include to make your email valuable:

Offer a strategy suggestion
Reference a mutual connection
Include a blog post about your product
Send a customer review

#### 6. What action do I want the recipient to take?
What’s the goal of this email? Do you want the prospect to reach out when they need more information or are you looking to set up a product demonstration?

Reps can often forget to include a call-to-action in their emails, making it difficult for the prospect to respond. Without giving prospects a defined next step, it can be a challenge to move forward.

Here are some examples of calls-to-action reps can include in their sales emails:

1. Include a calendar invite
1. Provide a case study and ask the buyer to read it
1. Ask when they’re free for a discussion
1. Send them to a piece of content for review
1. Ask a question about something they’ve written

The introductory email sets the tone for the relationship. Before you reach out, ask yourself these six questions to determine whether or not you’re fully prepared to send that email. If you can’t answer all of these questions, hit the reset button and get back to the drawing board.

### 4 Ways to Ruin a Sales Presentation <a name="presentation"></a>

A perfect sales presentation paints the product, the company, and the rep in an ideal light, while captivating the audience and turning a target company into a customer on the spot.

However, while a great meeting with a prospect can set you apart from your competitors, a bad presentation can do the same thing. Sales reps can often fall into the traps of a bad presentation without even realizing it. Everything is going smoothly in the rep’s mind ... until they realize the CEO has fallen asleep.

So how does a rep avoid this scenario? Below are four avoidable sales presentation mistakes to keep in mind for your next meeting.

#### 1. Being overly general.
Not personalizing the presentation is a mistake because every buyer is different. Each buyer has a unique pain point, and is in need of a unique solution. By failing to customize the presentation, the buyer can’t see themselves using your product. All the buyer knows is that the product works in general, or a different business used it and had success.

When we tell stories, the people listening feel as if what’s happening to the protagonist is happening to them too, according to studies. By personalizing the message, a sales rep can put the potential buyer into the shoes of another customer who has experienced success using the product, and help them visualize how the offering could help their company as well.

#### 2. Listing too many benefits.
Speaking about too many benefits can turn a prospect off because our brains can only process so much information at once. In fact, recent studies have found that consuming too much information can distract a person, resulting in a negative impact on personal well-being and decision making, among other things.

Instead of bragging about everything your product can do, sales reps should focus on three to four benefits that will solve a very specific pain point for the prospect. Studies have found that humans retain information best when they find it useful. By highlighting a few key, relevant benefits in your presentation, the prospect is more likely to retain the information, which will be critical when they make a decision.

#### 3. Slipping into autopilot.
Beginning a presentation with a monotone voice, bad body language, or an obviously scripted intro can kill your momentum in a hurry. According to Sherrie Bourg Carter, emotions are contagious. So if a sales rep lacks energy and enthusiasm during their presentation, this lackluster mood will likely wear off on the stakeholders in attendance.

There are a multitude of ways to avoid autopilot. By giving the presentation beforehand to coworkers, reps can discover where they need to improve and where the information could be strengthened. Another trick is to have a cup of coffee or work out the morning before the presentation to raise your energy. If you’re full of energy and enthusiastic about your product, these feelings can spread to your audience.

#### 4. Dodging questions.
Dodging questions after a sales presentation is a bad idea because it doesn’t allow the audience to clarify information, and according to 24Slides, can weaken your credibility. When prospects aren’t able to have specific questions answered, they can’t learn about what matters most to them. Instead of leaving the presentation feeling great about the product, the prospect is likely to leave with more questions than answers, which defeats the whole purpose of the presentation.

While reps might have a fear of answering questions after a presentation, it’s important to open the floor to the audience. Reps who take the extra time to answer tough questions can showcase what it’s like to work with them on an ongoing basis and the commitment they have to their prospects and customers. And remember, if you’re truly unsure, there’s nothing wrong with saying “I don’t understand” and moving forward.

A sales presentation can be tricky to master. Reps can give 1,000 of them but still have an off day every once in awhile. By focusing on reversing some bad habits, reps can ensure their prospects enjoy their presentations and find it valuable.

### Persistent or Just Plain Pushy? 7 Sales Behaviors Decoded <a name="behaviors"></a>

When was the last time you called a prospect and they said, “I’m so glad you called! I love salespeople!”?

Probably never.

#### Salespeople: Are You Being Persistent or Pushy?

Salespeople face a unique dimemma.  On one hand, tenacity is key to getting business -- 80% of sales requires five follow-ups to close.

On the other, coming off as annoying is a real concern.  Salespeople have a negative perception to overcome, and many buyers assume the worst.

In sales, the line between pushiness and persistence can be hard to walk.  Here is a list of pushy behaviors to avoid -- and what to do instead.

##### PROSPECTING

* Pushy: Cold calling witout doing research and delivering a cookie-cutter pitch

* Persistent: Following up on an inbound lead with an introduction customized to the prospect's activity and context.

##### CONNECTING

* Pushy: Pitching immediately without providing context on who you are and why you're calling
    
* Persistent: Providing a reason for your call (ideally the prospect's previous activity or a referral) and offerign to help.
    
##### NURTURING

* Pushy: Sending "just checking in" emails without providing new value
    
* Persistent: Providing your prospect witha  a strategically sequenced progression of new, helpful resources and insights
    
##### DISCOVERY

* Pushy: Rushing through a long list of discovery questions so you can "cover all your bases."
    
* Persistent: Diving deep into areas yur  prospect is interested in, letting conversation flow naturally toward their priorities, and following up with more discovery calles if necessary.
    
##### DEMONSTRATION

* Pushy: Attempting to cover every feature of your product at once.
    
* Persistent: Focusing your presentation only on features and benefits relevant to your prospect's needs.
    
##### OBJECTION HANDLING

* Pushy: Glossing over your prospect's concerns and not taking them seriously.
    
* Persistent: Creating a plan that accomodates your prospect's objections, and actively addresses their hesitations.
    
##### CLOSING

* Pushy: Trying to "slam a deal" through on your timeline and your terms.
    
* Persistent: Making sure your prospect's on track to achieve their goals and is ready to implement your product upon purchase.
    

Recognize that there’s a fine line between being annoying and tenacious. The same sales activities can be executed in very different ways -- some good, some bad.
